# +---------+
# | Stealth |
# +---------+
# A top-down stealth game in python curses

# use pads for scrolling
# show sight lines, and draw only what can be heard or directly seen
# move with numpad

# trying desperately to understand what's going on with TTY charsets
# import locale
# locale.setlocale(locale.LC_ALL, '')

# TTY support is a bit out there :/ https://askubuntu.com/questions/23610/how-to-enable-unicode-support-in-a-tty
# The Linux kernel maintainers do not accept patches for better Unicode support on 
# the console because the console is to be used as an emergency interface. 
# What needs to be done is write a terminal emulator for the Linux framebuffer that undertakes the support for Unicode. 
# Something like a 'getty' replacement. This aspect of open-source development did not receive yet much attention.

# curses does pretty well with half/fullwidth chars, but it's kinda annoying
#	https://en.wikipedia.org/wiki/Halfwidth_and_fullwidth_forms

# this is a goldmine:
#	https://misc.flogisoft.com/bash/tip_colors_and_formatting

import curses
#-----------------------------------------------------------------------------------
def oldline(stdscr,ay,ax,by,bx,style=f"{chr(9675)}{chr(9472)}{chr(9675)}"):
	# draws a line between (ax,ay) and (bx,by)
	h=by-ay
	w=bx-ax
	# set line char (these values assume a 2:1 h:w ratio for each cell)
	if h==0:	# avoid dbz error
		line_chr=chr(9472)	# -
	elif w==0:
		line_chr=chr(9474)	# |
	elif abs(w/h)>4.828:
		line_chr=chr(9472)	# -
	elif abs(h/w)>2.118:
		line_chr=chr(9474)	# |
	elif h*w>0:	# \ type
		if (w/h)>1.442:
			line_chr=chr(65340)	# \
		else:
			line_chr=chr(9586)	# \ full
	else:		# / type
		if (w/h)<-1.442:
			line_chr=chr(65245)	# /
		else:
			line_chr=chr(9585)	# / full
	# draw point at a
	stdscr.addstr(ay,ax,style[0])
	# draw line
	x,y=(ax,ay)
	if w*h==0:	dx,dy=(0,0)
	else:		dx,dy=(w/h,h/w)
	for i in range(max(h,w)):
		if h>w:
			y+=1
			x+=dx
		else:
			x+=1
			y+=dy
		stdscr.addstr(int(y),int(x),line_chr)	#disp char
	# draw point at b
	stdscr.addstr(by,bx,style[2])
#-----------------------------------------------------------------------------------	
def line(stdscr,ay,ax,by,bx):
	# copy of boxline except with \ or / as joiners to runners

	# always go from left to right - some internals rely on this.
	if ax>bx:	return line(stdscr,by,bx,ay,ax)	# XXX if include args that change start and end symbols, need to reverse here

	h=by-ay
	w=bx-ax
	joiner=("\\","/")[ay>by]
	# draw start and end points
	stdscr.addstr(ay,ax,'○')
	# draw line
	if h==0 and w==0:	return
	elif h==0:	hline(stdscr,ay,ax,by,bx)
	elif w==0:	vline(stdscr,ay,ax,by,bx)
	elif h>w:	# mostly vertical
		y=ay
		dy=h/w
		for x in range(ax,bx):
			y+=dy
			vline(stdscr,ay,x,int(y),x) #
			stdscr.addstr(int(y),x,corners)	# ┌┘ or └┐
			ay=int(y)	# move start up
	else:	# mostly horizontal
		x=ax
		dx=w/h
		i=(-1,1)[ay<by]
		for y in range(ay,by,i):
			x+=dx
			hline(stdscr,y,ax,y,int(x))
			stdscr.addstr(y,int(x),corners[1])	# ┌┘ or └┐
			stdscr.addstr(y+i,int(x),corners[0])	# ┌┘ or └┐
			ax=int(x)	# move start up

	#draw stop
	stdscr.addstr(by,bx,'○')

#-----------------------------------------------------------------------------------	
def vline(stdscr,ay,ax,by,bx):	# │
	# draw vertical at given points exclusive
	if ay>by: return vline(stdscr,by,bx,ay,ax) # all to avoid haveing to use min/max or direction
	for y in range(ay+1,by):
		stdscr.addstr(y,ax,'│')
#-----------------------------------------------------------------------------------
def hline(stdscr,ay,ax,by,bx):	# ─
	# draw horizontal at given points exclusive
	stdscr.addstr(ay,min(ax+1,bx+1),'─'*abs(bx-ax-1))
#-----------------------------------------------------------------------------------
def boxline(stdscr,ay,ax,by,bx):
	# ☑	line from a to b
	# ☐	respect lines that cross, add double mark or 3-way, etc.
	# ☐	user-specified line styles

	# always go from left to right - some internals rely on this.
	if ax>bx:	return boxline(stdscr,by,bx,ay,ax)	# XXX if include args that change start and end symbols, need to reverse here

	h=by-ay
	w=bx-ax
	corners=("└┐","┌┘")[ay>by]
	# draw start and end points
	stdscr.addstr(ay,ax,'○')
	# draw line
	if h==0 and w==0:	return
	elif h==0:	hline(stdscr,ay,ax,by,bx)
	elif w==0:	vline(stdscr,ay,ax,by,bx)
	elif h>w:	# mostly vertical
		y=ay
		dy=h/w
		for x in range(ax,bx):
			y+=dy
			vline(stdscr,ay,x,int(y),x) #
			stdscr.addstr(int(y),x,corners)	# ┌┘ or └┐
			ay=int(y)	# move start up
	else:	# mostly horizontal
		x=ax
		dx=w/h
		i=(-1,1)[ay<by]
		for y in range(ay,by,i):
			x+=dx
			hline(stdscr,y,ax,y,int(x))
			stdscr.addstr(y,int(x),corners[1])	# ┌┘ or └┐
			stdscr.addstr(y+i,int(x),corners[0])	# ┌┘ or └┐
			ax=int(x)	# move start up

	#draw stop
	stdscr.addstr(by,bx,'○')

#-----------------------------------------------------------------------------------
def main(stdscr):
	# Clear screen
	curses.curs_set(0)
	stdscr.clear()

	for y in range(0,55,5):
		boxline(stdscr,0,0,y,100)
	for x in range(0,110,10):
		boxline(stdscr,0,0,50,x)

	# boxline(stdscr,5,5,50,20)

	# hline(stdscr,30,30,30,100)

	# vline(stdscr,30,30,50,30)

	stdscr.refresh()
	stdscr.getkey()
	

curses.wrapper(main)