class Selection:
	def run(self):
		assert 0, "Selection.run() DNE"
	def next(self, input):
		assert 0, "Selection.next() DNE"

class StateMachine:
    def __init__(self, initialState):
        self.currentState = initialState
        self.currentState.run()
    # Template method:
    def run(self, input):
        self.currentState = self.currentState.next(input)
        self.currentState.run()


# use:

class Play(Selection):
	def run(self):
		print("play")
	def next(self, input):
		if input=="d":
			return options
		elif input=="s":
			return credits
		else:
			return self

class Options(Selection):
	def run(self):
		print("options")
	def next(self, input):
		if input=="a":
			return play
		elif input=="s":
			return quit
		else:
			return self

class Credits(Selection):
	def run(self):
		print("credits")
	def next(self, input):
		if input=="w":
			return play
		elif input=="d":
			return quit
		else:
			return self

class Quit(Selection):
	def run(self):
		print("quit")
	def next(self, input):
		if input=="w":
			return options
		elif input=="a":
			return credits
		else:
			return self



play=Play()
credits=Credits()
options=Options()
quit=Quit()

menu=StateMachine(play)

char=input()
while char in "wasd":
	menu.run(char)
	char=input()